// <copyright file="GlobalSuppressions.cs" company="KelltonTech">
//     Copyright (C) 2017  KelltonTech
// </copyright>

// This file is used by Code Analysis to maintain SuppressMessage attributes that are applied to this
// project. Project-level suppressions either have no target or are given a specific target and
// scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the Code Analysis results, point to
// "Suppress Message", and click "In Suppression File". You do not need to add suppressions to this
// file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "PrincipalCertificate.Common.Utility.#ToDataTable`1(System.Collections.Generic.List`1<!!0>)", Justification = "Needed this name")]