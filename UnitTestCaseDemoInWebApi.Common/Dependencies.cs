﻿// <copyright file="Dependencies.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech </copyright>

namespace UnitTestCaseDemoInWebApi.Common
{
    #region Namespace

    using BaseService;
    using Microsoft.Practices.Unity;

    #endregion Namespace

    /// <summary>
    /// This class is a static class to register all dependencies for the project
    /// </summary>
    public static class Dependencies
    {
        /// <summary>
        /// Register dependencies of current project
        /// </summary>
        public static void Register()
        {
           DIContainer.Instance.RegisterInstance(DIContainer.Instance);
        }       
    }
}