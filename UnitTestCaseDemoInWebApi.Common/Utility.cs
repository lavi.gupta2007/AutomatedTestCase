﻿// <copyright file="Utility.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.Common
{
    #region Namespace

    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;


    #endregion

    /// <summary>
    /// The Static Class to do some function
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// To the data table.
        /// </summary>
        /// <typeparam name="T">T Type</typeparam>
        /// <param name="items">The items.</param>
        /// <returns>Data Table</returns>
        public static DataTable ToDataTable<T>(IList<T> items)
        { 
            using (DataTable dataTable = new DataTable(typeof(T).Name))
            {
                dataTable.Locale = CultureInfo.InvariantCulture;
                PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in props)
                {                    
                    dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);       
                }

                if (items != null && items.Count > 0)
                {
                    foreach (T item in items)
                    {
                        var values = new object[props.Length];
                        for (int i = 0; i < props.Length; i++)
                        {
                            values[i] = props[i].GetValue(item, null);
                        }

                        dataTable.Rows.Add(values);
                    }
                }

                ////put a breakpoint here and check datatable
                return dataTable;
            }
        }

        /// <summary>
        /// Returns the maximal element of the given sequence, based on
        /// the given projection.
        /// </summary>
        /// <remarks>
        /// If more than one element has the maximal projected value, the first
        /// one encountered will be returned. This overload uses the default comparer
        /// for the projected type. This operator uses immediate execution, but
        /// only buffers a single result (the current maximal element).
        /// </remarks>
        /// <typeparam name="TSource">Type of the source sequence</typeparam>
        /// <typeparam name="TKey">Type of the projected element</typeparam>
        /// <param name="source">Source sequence</param>
        /// <param name="selector">Selector to use to pick the results to compare</param>
        /// <returns>The maximal element, according to the projection.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/> or <paramref name="selector"/> is null</exception>
        /// <exception cref="InvalidOperationException"><paramref name="source"/> is empty</exception>

        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> selector)
        {
            return source.MaxBy(selector, null);
        }

        /// <summary>
        /// Returns the maximal element of the given sequence, based on
        /// the given projection and the specified comparer for projected values. 
        /// </summary>
        /// <remarks>
        /// If more than one element has the maximal projected value, the first
        /// one encountered will be returned. This operator uses immediate execution, but
        /// only buffers a single result (the current maximal element).
        /// </remarks>
        /// <typeparam name="TSource">Type of the source sequence</typeparam>
        /// <typeparam name="TKey">Type of the projected element</typeparam>
        /// <param name="source">Source sequence</param>
        /// <param name="selector">Selector to use to pick the results to compare</param>
        /// <param name="comparer">Comparer to use to compare projected values</param>
        /// <returns>The maximal element, according to the projection.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="source"/>, <paramref name="selector"/> 
        /// or <paramref name="comparer"/> is null</exception>
        /// <exception cref="InvalidOperationException"><paramref name="source"/> is empty</exception>

        public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            
            comparer = comparer ?? Comparer<TKey>.Default;

            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence contains no elements");
                }
                var max = sourceIterator.Current;
                var maxKey = selector(max);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, maxKey) > 0)
                    {
                        max = candidate;
                        maxKey = candidateProjected;
                    }
                }
                return max;
            }
        }

        /// <summary>
        /// To the data table.
        /// </summary>
        /// <typeparam name="T">T Type</typeparam>
        /// <param name="items">The items.</param>
        /// <returns>The Data table</returns>
        public static DataTable ToDataTable<T>(T items)
        {
            using (DataTable dataTable = new DataTable(typeof(T).Name))
            {
                dataTable.Locale = CultureInfo.InvariantCulture;
                PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in props)
                {
                   dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);                  
                }

                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(items, null);
                }

                dataTable.Rows.Add(values);

                ////put a breakpoint here and check datatable
                return dataTable;
            }
        }

        /// <summary>
        /// To the collection.
        /// </summary>
        /// <typeparam name="T">T Type</typeparam>
        /// <param name="items">The items.</param>
        /// <returns>The Collection</returns>
        public static Collection<T> ToCollection<T>(this IList<T> items)
        {
            Collection<T> collection = new Collection<T>();

            if (items != null)
            {
                for (int i = 0; i < items.Count; i++)
                {
                    collection.Add(items[i]);
                }
            }

            return collection;
        }      

        /// <summary>
        /// To the collection.
        /// </summary>
        /// <typeparam name="T">T Type</typeparam>
        /// <param name="data">The data.</param>
        /// <returns>The Collection</returns>
        public static Collection<T> ToCollection<T>(this IEnumerable<T> data)
        {
            return new Collection<T>(data.ToList());
        }
    }
}