using System;
using System.Collections.Generic;

namespace UnitTestCaseDemoInWebApi.DataAccess.Models
{
    public partial class Employees
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
    }
}
