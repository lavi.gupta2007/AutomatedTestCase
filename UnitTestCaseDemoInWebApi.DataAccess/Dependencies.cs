﻿// <copyright file="Dependencies.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech </copyright>

namespace UnitTestCaseDemoInWebApi.DataAccess
{
    #region Namespace

    using BaseService;
    using Contracts;
    using Microsoft.Practices.Unity;

    #endregion Namespace

    /// <summary>
    /// This class is a static class to register all dependencies for the project
    /// </summary>
    public static class Dependencies
    {
        /// <summary>
        /// Registers this instance.
        /// </summary>
        public static void Register()
        {
            DIContainer.Instance.RegisterType<IEmployeedatabaseContext, EmployeedatabaseContext>();          
            DIContainer.Instance.RegisterType<IEmployeeRepository, EmployeeRepository>();
           
        }
    }
}