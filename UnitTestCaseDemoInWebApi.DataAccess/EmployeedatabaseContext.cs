// <copyright file="EmployeedatabaseContext.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Contracts;
    using Models;
    using Models.Mapping;

    /// <summary>
    /// The Employee Database Context Class
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    /// <seealso cref="UnitTestCaseDemoInWebApi.DataAccess.Contracts.IEmployeedatabaseContext" />
    public partial class EmployeedatabaseContext : DbContext, IEmployeedatabaseContext
    {
        #region Private Fields

        /// <summary>
        /// The minimum parameters length 
        /// </summary>
        private const int MINPARAMETERSLENGTH = 1;

        #endregion Private Fields

        #region Public Constructors

        static EmployeedatabaseContext()
        {
            Database.SetInitializer<EmployeedatabaseContext>(null);
        }

        public EmployeedatabaseContext()
            : base("Name=EmployeedatabaseContext")
        {
        }

        #endregion Public Constructors



        #region Public Properties

        /// <summary>
        /// Gets or sets the employees. 
        /// </summary>
        /// <value>
        /// The employees. 
        /// </value>
        public IDbSet<Employees> Employees { get; set; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources 
        /// </summary>
        public void DisposeObject()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Executes the routine. 
        /// </summary>
        /// <typeparam name="T">
        /// T Type 
        /// </typeparam>
        /// <param name="routineName">
        /// Name of the routine. 
        /// </param>
        /// <param name="parameters">
        /// The parameters. 
        /// </param>
        /// <returns>
        /// IEnumerable T type 
        /// </returns>
        public async Task<IEnumerable<T>> ExecuteRoutine<T>(string routineName, IDictionary<string, object> parameters)
        {
            var typeList = default(IEnumerable<T>);

            if (!string.IsNullOrEmpty(routineName))
            {
                var sqlParameters = new List<SqlParameter>();

                if (parameters != default(IDictionary<string, object>) && parameters.Count >= MINPARAMETERSLENGTH)
                {
                    foreach (var key in parameters.Keys)
                    {
                        sqlParameters.Add(new SqlParameter(key, parameters[key]));
                    }
                }

                typeList = Database.SqlQuery<T>(routineName, sqlParameters.ToArray()).ToList();
            }

            return await Task.FromResult(typeList);
        }

        /// <summary>
        /// Executes the routine by data table. 
        /// </summary>
        /// <typeparam name="T">
        /// T Type 
        /// </typeparam>
        /// <param name="routineName">
        /// Name of the routine. 
        /// </param>
        /// <param name="parameters">
        /// The parameters. 
        /// </param>
        /// <returns>
        /// IEnumerable T type 
        /// </returns>
        public Task<IEnumerable<T>> ExecuteRoutineByDataTable<T>(string routineName, SqlParameter[] parameters)
        {
            return this.ExecuteRoutinebydtAsync<T>(routineName, parameters);
        }

        /// <summary>
        /// Executes the routine by data table. 
        /// </summary>
        /// <typeparam name="T">
        /// T Type 
        /// </typeparam>
        /// <param name="routineName">
        /// Name of the routine. 
        /// </param>
        /// <param name="parameters">
        /// The parameters. 
        /// </param>
        /// <returns>
        /// IEnumerable T type 
        /// </returns>
        public Task<ICollection<T>> ExecuteRoutineByDataTableCollection<T>(string routineName, SqlParameter[] parameters)
        {
            return this.ExecuteRoutinebydtAsyncCollection<T>(routineName, parameters);
        }

        #endregion Public Methods



        #region Protected Methods

        /// <summary>
        /// Executes the reader. 
        /// </summary>
        /// <typeparam name="T">
        /// T Types 
        /// </typeparam>
        /// <param name="mapEntities">
        /// The map entities. 
        /// </param>
        /// <param name="exec">
        /// The execute. 
        /// </param>
        /// <param name="parameters">
        /// The parameters. 
        /// </param>
        /// <returns>
        /// T Type 
        /// </returns>
        protected virtual T ExecuteReader<T>(Func<DbDataReader, T> mapEntities, string exec, params object[] parameters)
        {
            EmployeedatabaseContext databaseContext = default(EmployeedatabaseContext);
            using (var conn = new SqlConnection(databaseContext.Database.Connection.ConnectionString))
            {
                using (var command = new SqlCommand(exec, conn))
                {
                    conn.Open();
                    command.Parameters.AddRange(parameters);
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        if (mapEntities != null)
                        {
                            T data = mapEntities(reader);
                            return data;
                        }
                    }
                }
            }

            return default(T);
        }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context. The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">
        /// The builder that defines the model for the context being created. 
        /// </param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created. The model for that context is then cached and is for all further instances of
        /// the context in the app domain. This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and
        /// DbContextFactory classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EmployeesMap());
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Executes the script non query. 
        /// </summary>
        /// <param name="script">
        /// The script. 
        /// </param>
        /// <returns>
        /// The Inserted Value 
        /// </returns>
        private static int ExecuteScriptNonQuery(string script)
        {
            using (EmployeedatabaseContext context = new EmployeedatabaseContext())
            {
                int result = 0;
                string connectionString = context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
                builder.ConnectTimeout = 2500;

                using (SqlConnection con = new SqlConnection(builder.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = script;
                        result = cmd.ExecuteNonQuery();
                        return result;
                    }
                }
            }
        }

        /// <summary>
        /// Executes the query asynchronous implementation. 
        /// </summary>
        /// <param name="routineName">
        /// Name of the routine. 
        /// </param>
        /// <param name="datasetName">
        /// Name of the dataset. 
        /// </param>
        /// <param name="parameters">
        /// The parameters. 
        /// </param>
        /// <returns>
        /// Data Set 
        /// </returns>
        private async Task<DataSet> ExecuteQueryAsyncImpl(string routineName, string datasetName, object[] parameters) // Not ref or out!
        {
            using (EmployeedatabaseContext context = new EmployeedatabaseContext())
            {
                DataSet ds = new DataSet(datasetName);
                string connectionString = context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
                builder.ConnectTimeout = 2500;
                SqlConnection con = new SqlConnection(builder.ConnectionString);
                SqlDataAdapter da = new SqlDataAdapter();
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = routineName;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddRange(parameters);
                    cmd.CommandTimeout = 6000;
                    da.SelectCommand = cmd;

                    da.Fill(ds);
                    return await Task.FromResult(ds);
                }
            }
        }

        /// <summary>
        /// Executes the routine by data table asynchronous. 
        /// </summary>
        /// <typeparam name="T">
        /// T Type 
        /// </typeparam>
        /// <param name="routineName">
        /// Name of the routine. 
        /// </param>
        /// <param name="parameters">
        /// The parameters. 
        /// </param>
        /// <returns>
        /// IEnumerable T type 
        /// </returns>
        private async Task<IEnumerable<T>> ExecuteRoutinebydtAsync<T>(string routineName, SqlParameter[] parameters)
        {
            var typeList = default(IEnumerable<T>);

            if (!string.IsNullOrEmpty(routineName))
            {
                var sqlParameters = new List<SqlParameter>();

                if (parameters != default(SqlParameter[]) &&
                    parameters.Length >= MINPARAMETERSLENGTH)
                {
                    foreach (SqlParameter p in parameters)
                    {
                        ////check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                        {
                            p.Value = DBNull.Value;
                        }

                        sqlParameters.Add(p);
                    }
                }

                typeList = Database.SqlQuery<T>(routineName, sqlParameters.ToArray()).ToList();
            }

            return await Task.FromResult(typeList);
        }

        /// <summary>
        /// Executes the routine by data table asynchronous. 
        /// </summary>
        /// <typeparam name="T">
        /// T Type 
        /// </typeparam>
        /// <param name="routineName">
        /// Name of the routine. 
        /// </param>
        /// <param name="parameters">
        /// The parameters. 
        /// </param>
        /// <returns>
        /// IEnumerable T type 
        /// </returns>
        private async Task<ICollection<T>> ExecuteRoutinebydtAsyncCollection<T>(string routineName, SqlParameter[] parameters)
        {
            var typeList = default(ICollection<T>);

            if (!string.IsNullOrEmpty(routineName))
            {
                var sqlParameters = new List<SqlParameter>();

                if (parameters != default(SqlParameter[]) &&
                    parameters.Length >= MINPARAMETERSLENGTH)
                {
                    foreach (SqlParameter p in parameters)
                    {
                        ////check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                        {
                            p.Value = DBNull.Value;
                        }

                        sqlParameters.Add(p);
                    }
                }

                typeList = Database.SqlQuery<T>(routineName, sqlParameters.ToArray()).ToList();
            }

            return await Task.FromResult(typeList);
        }

        #endregion Private Methods
    }
}