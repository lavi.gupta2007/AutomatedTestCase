﻿// <copyright file="IEmployeeRepository.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.DataAccess.Contracts
{
    #region Namespace

    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Threading.Tasks;
    using Entity;

    #endregion Namespace

    /// <summary>
    /// The Employee Repository Interface
    /// </summary>
    public interface IEmployeeRepository
    {   
        /// <summary>
        /// Lists the asynchronous.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>Employee Details</returns>
        Task<ICollection<Employees>> ListAsync(int filter);

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="employee">The Employee.</param>
        /// <returns>
        /// The Employee
        /// </returns>
        Task<IEnumerable<Employees>> CreateAsync(Employees employee);

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="employee">The Employee.</param>
        /// <returns>
        /// To Update Employee
        /// </returns>
        Task<IEnumerable<Employees>> UpdateAsync(Employees employee);

        /// <summary>
        /// Deletes the employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Nothing </returns>
        Task<string> DeleteEmployee(int id); 
    }
}
