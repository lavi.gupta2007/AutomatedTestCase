﻿// <copyright file="IEmployeedatabaseContext.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.DataAccess.Contracts
{
    #region Namespace

    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using Models;

    #endregion Namespace

    /// <summary>
    /// The Now Social Order Interface
    /// </summary>
    public interface IEmployeedatabaseContext
    {
        /// <summary>
        /// Gets or sets the employees.
        /// </summary>
        /// <value>
        /// The employees.
        /// </value>
        IDbSet<Employees> Employees { get; set; }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        void DisposeObject();

        /// <summary>
        /// Saves the changes asynchronous.
        /// </summary>
        /// <returns>Save Changes.</returns>
        Task<int> SaveChangesAsync();     

        /// <summary>
        /// Executes the routine.
        /// </summary>
        /// <typeparam name="T">T type</typeparam>
        /// <param name="routineName">Name of the routine.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>IEnumerable T type</returns>
        Task<IEnumerable<T>> ExecuteRoutine<T>(string routineName, IDictionary<string, object> parameters);

        /// <summary>
        /// Executes the routine by data table.
        /// </summary>
        /// <typeparam name="T">T type</typeparam>
        /// <param name="routineName">Name of the routine.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>IEnumerable T type</returns>
        Task<IEnumerable<T>> ExecuteRoutineByDataTable<T>(string routineName, SqlParameter[] parameters);

        /// <summary>
        /// Executes the routine by data table.
        /// </summary>
        /// <typeparam name="T">T type</typeparam>
        /// <param name="routineName">Name of the routine.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>IEnumerable T type</returns>
        Task<ICollection<T>> ExecuteRoutineByDataTableCollection<T>(string routineName, SqlParameter[] parameters);
    }
}