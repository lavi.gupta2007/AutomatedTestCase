﻿// <copyright file="EmployeeRepository.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.DataAccess
{
    #region Namespace

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using Common;
    using Contracts;
    using Entity;
    using Logging;

    #endregion

    /// <summary>
    /// Employee Repository
    /// </summary>    
    public class EmployeeRepository : IEmployeeRepository
    {
        #region Private Members

        /// <summary>
        /// Holds the reference of type Employee Database Context
        /// </summary>
        private IEmployeedatabaseContext context;

        #endregion Private Members

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public EmployeeRepository(IEmployeedatabaseContext context)
        {
            this.context = context;
        }

        #endregion Constructors

        #region Public Methods

        /// <summary>
        /// Lists the asynchronous.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>
        /// List of Employees Details
        /// </returns>
        public async Task<ICollection<Employees>> ListAsync(int filter)
        {
            var employeeCollection = default(IEnumerable<Employees>);
            try
            {
                var procedureName = "EXEC USP_GetEmployees @Id";
                var sqlParameters = new Dictionary<string, object>
                {
                   { "@Id", filter },                 
                   
                };

                employeeCollection = await this.context.ExecuteRoutine<Employees>(
                      procedureName, sqlParameters);
            }
            catch (Exception exception)
            {
                LogManager.Write(exception);
            }


            return employeeCollection.ToCollection();
        }
       
        /// <summary>
        /// Creates the asynchronous.
        /// </summary>        
        /// <param name="employees">The employee.</param>
        /// <returns>Create the employee</returns>
        public async Task<IEnumerable<Employees>> CreateAsync(Employees employees)
        {
            var employeesDetails = default(IEnumerable<Employees>);
            try
            {
                DataTable dt = Common.Utility.ToDataTable(employees);

                var procedureName = "EXEC usp_InsertUpdateEmployee @EmployeeDetail";

                SqlParameter[] param = new SqlParameter[1];

                param[0] = new SqlParameter("@EmployeeDetail", dt);
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].TypeName = "dbo.EmployeeUDT";

                employeesDetails = await this.context.ExecuteRoutineByDataTable<Employees>(procedureName, param);
            }
            catch (Exception exception)
            {
                LogManager.Write(exception);
            }
            finally
            {
                this.context.DisposeObject();
            }

            return await Task.FromResult(employeesDetails);
        }


        /// <summary>
        /// Creates the asynchronous.
        /// </summary>        
        /// <param name="employees">The employee.</param>
        /// <returns>Updated employee</returns>
        public async Task<IEnumerable<Employees>> UpdateAsync(Employees employees)
        {
            var employeesDetails = default(IEnumerable<Employees>);
            try
            {
                DataTable dt = Common.Utility.ToDataTable(employees);

                var procedureName = "EXEC usp_InsertUpdateEmployee @EmployeeDetail";

                SqlParameter[] param = new SqlParameter[1];

                param[0] = new SqlParameter("@EmployeeDetail", dt);
                param[0].SqlDbType = SqlDbType.Structured;
                param[0].TypeName = "dbo.EmployeeUDT";

                employeesDetails = await this.context.ExecuteRoutineByDataTable<Employees>(procedureName, param);
            }
            catch (Exception exception)
            {
                LogManager.Write(exception);
            }
            finally
            {
                this.context.DisposeObject();
            }

            return await Task.FromResult(employeesDetails);
        }

        /// <summary>
        /// Delete the Employee Data.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Delete employee
        /// </returns>
        public async Task<string> DeleteEmployee(int id)
        {

            string result = string.Empty;

            try
            {
             
                var procedureName = "EXEC usp_DeleteEmployee @EmployeeId";
                var sqlParameters = new Dictionary<string, object>
                {
                   { "@EmployeeId", id }                 
                   
                };

             
                await this.context.ExecuteRoutine<string>( procedureName, sqlParameters);
                return await Task.FromResult("Deleted");
            }
            catch (Exception exception)
            {
                LogManager.Write(exception);
            }
          
            return null;
        }

        #endregion           
    }
}
