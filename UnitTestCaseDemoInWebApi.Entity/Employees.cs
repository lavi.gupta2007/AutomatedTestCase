﻿// <copyright file="EmployeeDataTest.cs" company="kelltonTech">Copyright (C) 2017 kelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.Entity
{
    using Newtonsoft.Json;

    public class Employees
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        /// <value>
        /// The age.
        /// </value>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Age { get; set; }
    }
}
