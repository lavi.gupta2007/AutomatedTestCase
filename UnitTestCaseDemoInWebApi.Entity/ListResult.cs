﻿// <copyright file="ListResult.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech </copyright>

namespace UnitTestCaseDemoInWebApi.Entity
{
    #region namespaces

    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    #endregion namespaces

    /// <summary>
    /// Holds the list type result response
    /// </summary>
    /// <typeparam name="T">Type of object</typeparam>
    public class ListResult<T>
    {
        /// <summary>
        /// The records
        /// </summary>
        private ICollection<T> items = new Collection<T>();

        /// <summary>
        /// Gets the records.
        /// </summary>
        /// <value>
        /// The records.
        /// </value>
        public ICollection<T> Items
        {
            get
            {
                return this.items;
            }
        }

        /// <summary>
        /// Gets or sets the total results.
        /// </summary>
        /// <value>
        /// The total results.
        /// </value>
        public int TotalResults { get; set; }

        /// <summary>
        /// Assigns the records.
        /// </summary>
        /// <param name="itemsData">The records.</param>
        public void AssignItems(ICollection<T> itemsData)
        {
            this.items = itemsData;
        }
    }
}