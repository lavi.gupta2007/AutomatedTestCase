﻿// <copyright file="WebApiTest.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.ReferenceDataService.Tests
{
    #region Namespace

    using System;
    using System.Web.Http;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    #endregion Namespace

    /// <summary>
    /// WebApiTest class.
    /// </summary>
    [TestClass]
    public class WebApiTest : IDisposable
    {
        /// <summary>
        /// The configuration
        /// </summary>
        private readonly HttpConfiguration config = new HttpConfiguration();

        /// <summary>
        /// Registers the configuration test.
        /// </summary>
        [TestMethod]
        public void RegisterConfigTest()
        {
            WebApiConfig.Register(this.config);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #region Protected Methods

        /// <summary>
        /// Disposes resources
        /// </summary>
        /// <param name="disposing">Flag to dispose or not</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.config.Dispose();
            }
        }

        #endregion Protected Methods
    }
}