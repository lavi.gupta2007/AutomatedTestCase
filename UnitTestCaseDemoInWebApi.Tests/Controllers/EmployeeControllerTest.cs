﻿// <copyright file="EmployeeControllerTest.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.Tests.Controllers
{

    #region Namespace

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;
    using DataAccess;
    using Entity;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using UnitTestCaseDemoInWebApi.Business;
    using UnitTestCaseDemoInWebApi.Controllers;
    using UnitTestCaseDemoInWebApi.DataAccess.Contracts;

    #endregion Namespace

    [TestClass]
    public class EmployeeControllerTest : IDisposable
    {

        #region Public Fields

        /// <summary>
        /// The data adapter 
        /// </summary>
        public readonly Mock<EmployeedatabaseContext> mockDataAdapter;

        #endregion Public Fields

        #region Private   

        /// <summary>
        /// The Employees list 
        /// </summary>
        private readonly ListResult<Employees> employeesList = new ListResult<Employees>();

        /// <summary>
        /// The employees list create
        /// </summary>
        private readonly ListResult<Employees> employeesListCreate = new ListResult<Employees>();

        /// <summary>
        /// The employees list delete
        /// </summary>
        private readonly ListResult<Employees> employeesListDelete = new ListResult<Employees>();

        /// <summary>
        /// The controller
        /// </summary>
        private readonly EmployeeController controller = null;

        /// <summary>
        /// The manager
        /// </summary>
        private readonly EmployeeManager manager;

        /// <summary>
        /// The data adapter 
        /// </summary>
        private readonly Mock<IEmployeeRepository> mockEmployeeRepository;     

        ICollection<Employees> employeeData;

        #endregion Private

        /// <summary>
        /// Initializes a new instance of the <see cref="BrandsControllerTests"/> class.
        /// </summary>
        public EmployeeControllerTest()
        {    
            this.mockDataAdapter = new Mock<EmployeedatabaseContext>() { CallBase = true };
            this.mockEmployeeRepository = new Mock<IEmployeeRepository>();
            this.manager = new EmployeeManager(new EmployeeRepository(this.mockDataAdapter.Object));
            this.controller = new EmployeeController(this.manager);
            this.controller.Request = new HttpRequestMessage();
            this.controller.Configuration = new HttpConfiguration();
            Common.Dependencies.Register();
            Business.Dependencies.Register();
            var httpRequest = new HttpRequest(string.Empty, "http://localhost:51007/api/Employee", string.Empty);
            using (var stringWriter = new StringWriter(CultureInfo.CurrentCulture))
            {
                var httpResponse = new HttpResponse(stringWriter);
                HttpContext.Current = new HttpContext(httpRequest, httpResponse);
            }
        }

        /// <summary>
        /// Brands the controller test.
        /// </summary>
        /// <returns>the task</returns>
        [TestMethod]
        public async Task ApiListTest()
        {
            try
            {
                this.SetupDataEmployees();
                var response = await this.controller.Get(0);
                var resultContecnt = await response.ExecuteAsync(new CancellationToken(false));
                
                ICollection<Employees> employeeData;
                Assert.IsTrue(resultContecnt.TryGetContentValue<ICollection<Employees>>(out employeeData));                
                Assert.IsTrue(employeeData.Where(x => !this.employeesList.Items.ToList().Any(y => y.Id.Equals(x.Id))).ToList().Count > 0 ? true : false);
                Assert.IsNotNull(employeeData.FirstOrDefault(employeeItem => this.employeesList.Items.Any(item => employeeItem.Name.ToString().Equals(item.Name.ToString(), StringComparison.OrdinalIgnoreCase))));
            }
            finally
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// Brands the controller test.
        /// </summary>
        /// <returns>the task</returns>
        [TestMethod]
        public async Task ApiGetByIdTest()
        {
            try
            {
                this.SetupDataEmployees();
                var response = await this.controller.Get(1);
                var resultContecnt = await response.ExecuteAsync(new CancellationToken(false));                
                ICollection<Employees> employeeData;
                Assert.IsTrue(resultContecnt.TryGetContentValue<ICollection<Employees>>(out employeeData));
                Assert.AreEqual(1,employeeData.Count);

                Assert.IsTrue(employeeData.Where(x => this.employeesList.Items.ToList().Any(y => y.Id.Equals(x.Id))).ToList().Count > 0 ? true : false);
                Assert.IsNotNull(employeeData.FirstOrDefault(employeeItem => this.employeesList.Items.Any(item => employeeItem.Name.ToString().Equals(item.Name.ToString(), StringComparison.OrdinalIgnoreCase))));
            }
            finally
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// Employees the create asynchronous test.
        /// </summary>
        /// <returns>Create Employee Test</returns>
        [TestMethod]
        public async Task ApiEmployeeCreateAsyncTest()
        {          
            var response = await this.controller.Get(0);
            var resultContent = await response.ExecuteAsync(new CancellationToken(false));
            resultContent.TryGetContentValue<ICollection<Employees>>(out employeeData);
            this.SetupCreateData();
            var employee = new Employees
            {
                Name = "ApiLayerTest",
                Email = "ApiLayerTest@gmail.com",
                Age = 27
            };
            IEnumerable<Employees> employeeDataCreated;
            var createResponse = await this.controller.Post(employee);
            var resultContecnt = await createResponse.ExecuteAsync(new CancellationToken(false));
            resultContecnt.TryGetContentValue<IEnumerable<Employees>>(out employeeDataCreated);

            var itemPrevious = employeeData.MaxBy(x => x.Id);
            var itemCreated = employeeDataCreated.MaxBy(x => x.Id);            
            Assert.AreNotEqual((itemPrevious.Id), itemCreated.Id);
            Assert.IsTrue(itemPrevious.Id < itemCreated.Id);
        }

        /// <summary>
        /// Deletes the asynchronous test.
        /// </summary>
        /// <returns>the task</returns>
        [TestMethod]
        public async Task ApiDeleteEmployeeAsyncTest()
        {
            this.SetUpDataForDelete();

            var itemToRemove = this.employeesListDelete.Items.Single(r => r.Id == 1);
            this.employeesListDelete.Items.Remove(itemToRemove);

            var deleteResponse = await this.controller.Delete(1);
            var resultContecnt = await deleteResponse.ExecuteAsync(new CancellationToken(false));


            var response = await this.controller.Get(1);
            var resultContent = await response.ExecuteAsync(new CancellationToken(false));
            resultContent.TryGetContentValue<ICollection<Employees>>(out employeeData);           

            var employessUpdatedItems = this.employeesListDelete.Items.FirstOrDefault(item => item.Id.Equals(1));

            Assert.IsNull(employessUpdatedItems);
            Assert.AreEqual(0, employeeData.Count);
        }


         /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }     

        #region Protected Methods

        /// <summary>
        /// Disposes resources
        /// </summary>
        /// <param name="disposing">Flag to dispose or not</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.controller != null)
                {
                    this.controller.Request.Dispose();
                    this.controller.Dispose();
                }
            }
        }

        #endregion Protected Methods

        /// <summary>
        /// Setups the manager. 
        /// </summary>
        private void SetupDataEmployees()
        {
            this.employeesList.Items.Add(new Employees
            {
                Id = 1,
                Name = "ApiLayerTest",
                Email = "ApiLayerTest@gmail.com",
                Age = 27
            }
           );            
                this.employeesList.TotalResults = this.employeesList.Items.Count;

                this.mockEmployeeRepository.Setup(data => data.ListAsync(It.IsAny<int>())).Returns(this.EmployeeListAsync(0));          
           
        }

        /// <summary>
        /// Create setups the manager.
        /// </summary>
        private void SetupCreateData()
        {
            this.mockEmployeeRepository.Setup(p => p.CreateAsync(It.IsAny<Employees>())).Returns(this.EmployeeCreateListAsync(employeeData));
        }

        /// <summary>
        /// Setups the manager.
        /// </summary>
        private void SetUpDataForDelete()
        {
            this.employeesListDelete.AssignItems(new List<Employees> { new Employees { Id = 1 }, new Employees { Id = 2 }, new Employees { Id = 3 } });
            this.employeesListDelete.TotalResults = this.employeesListDelete.Items.Count;
            string id = Convert.ToString(this.employeesListDelete.Items.Select(employee => employee.Id), CultureInfo.CurrentCulture);
            this.mockEmployeeRepository.Setup(p => p.DeleteEmployee(1));
        }

        /// <summary>
        /// Employees the list asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The Employee List</returns>
        private Task<ICollection<Employees>> EmployeeListAsync(int id)
        {
            var retValue = new ListResult<Employees>();
            if (id >= 0)
            {
                retValue.AssignItems(this.employeesList.Items.ToList());
                retValue.TotalResults = retValue.Items.Count;
            }

            return Task.FromResult(retValue.Items);
        }

        /// <summary>
        /// Employees the list asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The Employee List</returns>
        private Task<IEnumerable<Employees>> EmployeeCreateListAsync(ICollection<Employees> employeeData)
        {
            var retValue = new ListResult<Employees>();
            if (employeeData.Count > 0)
            {
                retValue.AssignItems(this.employeesList.Items.ToList());
                retValue.TotalResults = retValue.Items.Count;
            }

            return Task.FromResult(retValue.Items as IEnumerable<Employees>);
        }
    }
}
