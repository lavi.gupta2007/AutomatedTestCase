﻿// <copyright file="AuthenticationAttribute.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.Filters
{
    #region Namespaces

    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Web.Configuration;
    using System.Web.Http.Filters;     

    #endregion Namespaces

    /// <summary>
    /// This class Authenticates request.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class AuthenticationAttribute : ActionFilterAttribute
    {
        #region Constant

        /// <summary>
        /// The authentication token missing
        /// </summary>
        private const string AuthenticationTokenMissing = "Authentication Token not Supplied"; 

        #endregion Constant

        #region Private Fields

        /// <summary>
        /// The authentication token
        /// </summary>
        private string authToken = WebConfigurationManager.AppSettings["AuthToken"].ToString();

        /// <summary>
        /// The authentication token value
        /// </summary>
        private string authTokenValue = WebConfigurationManager.AppSettings["AuthTokenValue"].ToString();  

        #endregion

        #region Public 

        /// <summary>
        /// Occurs before the action method is invoked.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (actionContext != null)
            {
                if (!actionContext.Request.Headers.Contains(this.authToken) || !this.authTokenValue.Equals(actionContext.Request.Headers.GetValues(this.authToken).FirstOrDefault(), StringComparison.OrdinalIgnoreCase))
                {
                    actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                    actionContext.Response.Content = new StringContent(AuthenticationTokenMissing);
                    return;
                }

                base.OnActionExecuting(actionContext);
            }
        } 

        #endregion Public
    }    
}