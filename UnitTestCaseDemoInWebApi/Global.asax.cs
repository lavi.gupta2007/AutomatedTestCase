﻿// <copyright file="Global.asax.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech </copyright>

namespace UnitTestCaseDemoInWebApi
{
    using System;
    using System.Web;
    using System.Web.Http;
    using BaseService;
    using Logging;
   
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);
            Common.Dependencies.Register();
            Business.Dependencies.Register();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityResolver(DIContainer.Instance);
            LogManager.Initialize("errorOnly");  
        }
    }
}