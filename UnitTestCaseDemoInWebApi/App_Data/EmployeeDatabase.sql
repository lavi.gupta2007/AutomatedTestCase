USE [master]
GO
/****** Object:  Database [Employeedatabase]    Script Date: 7/14/2017 11:34:14 PM ******/
CREATE DATABASE [Employeedatabase]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Employeedatabase', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\Employeedatabase.mdf' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Employeedatabase_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\Employeedatabase_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Employeedatabase] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Employeedatabase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Employeedatabase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Employeedatabase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Employeedatabase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Employeedatabase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Employeedatabase] SET ARITHABORT OFF 
GO
ALTER DATABASE [Employeedatabase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Employeedatabase] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Employeedatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Employeedatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Employeedatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Employeedatabase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Employeedatabase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Employeedatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Employeedatabase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Employeedatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Employeedatabase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Employeedatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Employeedatabase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Employeedatabase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Employeedatabase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Employeedatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Employeedatabase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Employeedatabase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Employeedatabase] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Employeedatabase] SET  MULTI_USER 
GO
ALTER DATABASE [Employeedatabase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Employeedatabase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Employeedatabase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Employeedatabase] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Employeedatabase]
GO
/****** Object:  UserDefinedTableType [dbo].[EmployeeUDT]    Script Date: 7/14/2017 11:34:14 PM ******/
CREATE TYPE [dbo].[EmployeeUDT] AS TABLE(
	[Id] [int] NULL,
	[Name] [varchar](200) NULL,
	[Email] [varchar](300) NULL,
	[Age] [int] NULL
)
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEmployee]    Script Date: 7/14/2017 11:34:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[usp_DeleteEmployee] 	  
@EmployeeId int 	       
AS 

/*            
======================================================================================================================================

Copyright : kellton tech, 2017            
Author  : Lavi Gupta          
Create date : 13-July-2017            
Description : To Delete the Employees details

======================================================================================================================================        
Change Log  : 
--------------------------------------------------------------------------------------------------------------------------------------*/

BEGIN
SET NOCOUNT ON;
BEGIN TRY
BEGIN TRAN;


delete from Employees where Id= @EmployeeId


COMMIT TRAN;
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
BEGIN
ROLLBACK TRANSACTION;
END;


RETURN;

END CATCH;
SET NOCOUNT OFF;
END;



GO
/****** Object:  StoredProcedure [dbo].[USP_GetEmployees]    Script Date: 7/14/2017 11:34:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Prabhakar>
-- Create date: <Create Date,24-Nov-2016,>
-- Description:	<Description,GetRetailer>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetEmployees] --'JAGDISH'
	
 @Id int

WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;
    

	  

	SELECT * from Employees pc
	
	WHERE ((@Id = '') OR (@Id <> '' AND pc.Id = @Id))
 
    ORDER BY Name

END

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUpdateEmployee]    Script Date: 7/14/2017 11:34:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[usp_InsertUpdateEmployee] 
	  
@EmployeeDetail [dbo].EmployeeUDT READONLY 
	       
AS 

/*            
============================================================================================================================================            
Copyright : kellton tech, 2017            
Author  : Lavi Gupta          
Create date : 13-July-2017            
Description : To insert/update the Employees details
============================================================================================================================================            
Change Log  :       

--------------------------------------------------------------------------------------------------------------------------------------------            
*/

BEGIN
SET NOCOUNT ON;
	BEGIN TRY

		BEGIN TRAN;
		 DECLARE @OutputTbl TABLE (
                                      ID int
                                      );
            
		declare @id int =(Select top 1 id from @EmployeeDetail )



MERGE dbo.Employees CD
USING @EmployeeDetail bo
ON Cd.ID = bo.ID
WHEN MATCHED
THEN UPDATE
SET	CD.Name = bo.Name
,CD.Email = bo.Email
,CD.Age = bo.Age
			
WHEN NOT MATCHED BY TARGET
THEN INSERT (Name, Email,Age)

VALUES (bo.Name, bo.Email, bo.Age)
 OUTPUT INSERTED.Id
		  INTO @OutputTbl(Id);

		 
		  if(@id is NULL)
		  begin

		  set  @id=  (select tbl.Id
		  FROM @OutputTbl AS tbl)

		  end
		
		  select * from Employees where Id=@id


COMMIT TRAN;
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
BEGIN
ROLLBACK TRANSACTION;
END;


RETURN;

END CATCH;
SET NOCOUNT OFF;
END;



GO
/****** Object:  Table [dbo].[Employees]    Script Date: 7/14/2017 11:34:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Email] [varchar](300) NULL,
	[Age] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([Id], [Name], [Email], [Age]) VALUES (1, N'ApiLayerTest', N'ApiLayerTest@gmail.com', 27)
INSERT [dbo].[Employees] ([Id], [Name], [Email], [Age]) VALUES (2, N'BusinessLayerTest', N'BusinessLayerTest@gmail.com', 29)
INSERT [dbo].[Employees] ([Id], [Name], [Email], [Age]) VALUES (6, N'DataLayerTest', N'DataLayerTest@gmail.com', 29)
INSERT [dbo].[Employees] ([Id], [Name], [Email], [Age]) VALUES (7, N'BusinessLayerTest', N'BusinessLayerTest@gmail.com', 29)
INSERT [dbo].[Employees] ([Id], [Name], [Email], [Age]) VALUES (8, N'ApiLayerTest', N'ApiLayerTest@gmail.com', 27)
INSERT [dbo].[Employees] ([Id], [Name], [Email], [Age]) VALUES (9, N'DataLayerTest', N'DataLayerTest@gmail.com', 29)
SET IDENTITY_INSERT [dbo].[Employees] OFF
USE [master]
GO
ALTER DATABASE [Employeedatabase] SET  READ_WRITE 
GO
