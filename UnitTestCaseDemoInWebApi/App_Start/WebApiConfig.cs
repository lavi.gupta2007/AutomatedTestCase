﻿// <copyright file="WebApiConfig.cs" company="KelltonTech">
//     Copyright (C) 2017  KelltonTech
// </copyright>

namespace UnitTestCaseDemoInWebApi
{
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Routing;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );          
        }
    }
}
