﻿// <copyright file="EmployeeController.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech</copyright>

namespace UnitTestCaseDemoInWebApi.Controllers
{
    #region Namespace

    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using BaseService;
    using Business.Contracts;
    using Entity;
    using Filters;

    #endregion Namespace

    [Authentication]
    public class EmployeeController : ApiControllerBase
    {
        #region Private Members

        /// <summary>
        /// The employee manager
        /// </summary>
        private readonly IEmployeeManager employeeManager;
      
        #endregion Private Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeController"/> class.
        /// </summary>
        /// <param name="employeeManager">The employee manager.</param>
        public EmployeeController(IEmployeeManager employeeManager)
        {
            this.employeeManager = employeeManager;           
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Retrieves the specified filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>Employee Details</returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(int filter)
        {
                return await this.RetrieveAsync<ICollection<Employees>>(async operationResult =>
                {
                    operationResult.Content = await this.employeeManager.ListAsync(filter);
                });
        }        

        /// <summary>
        /// Posts the specified user identifier.
        /// </summary>  
        /// <param name="employees">The employees.</param>
        /// <returns>The Employees</returns>
        [HttpPost]
        public async Task<IHttpActionResult> Post(Employees employees)
        {
            return await this.RetrieveAsync<IEnumerable<Employees>>(async operationResult => 
            {
                operationResult.Content = await this.employeeManager.CreateAsync(employees);
            });              
        }

        /// <summary>
        /// Puts the specified employees.
        /// </summary>
        /// <param name="employees">The employees.</param>
        /// <returns> The Updated Employee </returns>
        [HttpPut]
        public async Task<IHttpActionResult> Put(Employees employees)
        {
            return await this.RetrieveAsync<IEnumerable<Employees>>(async operationResult =>
            {
                operationResult.Content = await this.employeeManager.UpdateAsync(employees);
            });
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns> Delete the employee </returns>
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {            
            return await this.DeleteAsync(async operationResult =>
            {
                var result = await this.employeeManager.DeleteEmployee(id);
                if (result != string.Empty)
                {
                    operationResult.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    operationResult.StatusCode = HttpStatusCode.NotFound;
                }
            });         
        }

        #endregion
    }
}
