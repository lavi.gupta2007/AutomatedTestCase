﻿// <copyright file="EmployeeManagerTest.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech </copyright>

namespace UnitTestCaseDemoInWebApi.Business.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Business;
    using DataAccess;
    using DataAccess.Contracts;
    using Entity;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Common;
    using System.Collections;
    using System.Globalization;

    [TestClass]
    public class EmployeeManagerTest
    {
        #region Public Fields

        /// <summary>
        /// The data adapter 
        /// </summary>
        public readonly Mock<EmployeedatabaseContext> mockDataAdapter;

        #endregion Public Fields

        #region Private Fields    

        /// <summary>
        /// The business layer 
        /// </summary>
        private readonly EmployeeManager businessLayer;

        /// <summary>
        /// The data adapter 
        /// </summary>
        private readonly Mock<IEmployeeRepository> mockEmployeeRepository;

        /// <summary>
        /// The Employees list 
        /// </summary>
        private readonly ListResult<Employees> employeesList = new ListResult<Employees>();

        /// <summary>
        /// The employees list create
        /// </summary>
        private readonly ListResult<Employees> employeesListCreate = new ListResult<Employees>();

        /// <summary>
        /// The employees list delete
        /// </summary>
        private readonly ListResult<Employees> employeesListDelete = new ListResult<Employees>();

        ICollection<Employees> employeeData;

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeManagerTest" /> class. 
        /// </summary>
        public EmployeeManagerTest()
        {
            this.mockDataAdapter = new Mock<EmployeedatabaseContext>() { CallBase = true };
            this.mockEmployeeRepository = new Mock<IEmployeeRepository>();
            this.businessLayer = new EmployeeManager(new EmployeeRepository(this.mockDataAdapter.Object));
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Employees the list asynchronous test.
        /// </summary>
        /// <returns>Employee List Test</returns>
        [TestMethod]
        public async Task BusinessEmployeeListAsyncTest()
        {
            this.SetupDataEmployees();   
            var result = await this.businessLayer.ListAsync(0);
            Assert.IsTrue(result.Except(this.employeesList.Items.ToList()).Any());
            Assert.IsTrue(result.Where(x => !this.employeesList.Items.ToList().Any(y => y.Email.Equals(x.Email))).ToList().Count > 0 ? true : false);
            Assert.IsNotNull(result.FirstOrDefault(employeeItem => this.employeesList.Items.Any(item => employeeItem.Name.ToString().Equals(item.Name.ToString(), StringComparison.OrdinalIgnoreCase))));
        }

        /// <summary>
        /// Employees the by identifier asynchronous test.
        /// </summary>
        /// <returns>Employee Id Test</returns>
        [TestMethod]
        public async Task BusinessEmployeeByIdAsyncTest()
        {
            this.SetupDataEmployees();           
            var result = await this.businessLayer.ListAsync(1);
            Assert.IsTrue(result.Except(this.employeesList.Items.ToList()).Any());
            Assert.IsNotNull(result.FirstOrDefault(employeeItem => this.employeesList.Items.Any(item => employeeItem.Email.ToString().Equals(item.Email.ToString(), StringComparison.OrdinalIgnoreCase))));
        }

        /// <summary>
        /// Employees the create asynchronous test.
        /// </summary>
        /// <returns>Create Employee Test</returns>
        [TestMethod]
        public async Task BusinessEmployeeCreateAsyncTest()
        {
            employeeData = await this.businessLayer.ListAsync(0);
            this.SetupCreateData();

            var employee = new Employees
            {                    
                Name = "BusinessLayerTest",
                Email = "BusinessLayerTest@gmail.com",
                Age = 29
            };

            var result = await this.businessLayer.CreateAsync(employee);
            var itemPrevious = employeeData.MaxBy(x => x.Id);
            var itemCreated = result.MaxBy(x => x.Id);
            Assert.IsNotNull(result);
            Assert.AreNotEqual((itemPrevious.Id ), itemCreated.Id);
            Assert.IsTrue(itemPrevious.Id<itemCreated.Id);

        }

        /// <summary>
        /// Deletes the asynchronous test.
        /// </summary>
        /// <returns>the task</returns>
        [TestMethod]
        public async Task BusinessDeleteAsyncTest()
        {
            this.SetUpDataForDelete();

            var itemToRemove = this.employeesListDelete.Items.Single(r => r.Id == 2);
            this.employeesListDelete.Items.Remove(itemToRemove);
            await this.businessLayer.DeleteEmployee(2);
            employeeData = await this.businessLayer.ListAsync(2);

            var employessUpdatedItems = this.employeesListDelete.Items.FirstOrDefault(item => item.Id.Equals(2));

            Assert.IsNull(employessUpdatedItems);
            Assert.AreEqual(0,employeeData.Count);
        }

        #endregion Public Methods

        #region Private Methods        

        /// <summary>
        /// Setups the manager. 
        /// </summary>
        private void SetupDataEmployees()
        {
            this.employeesList.Items.Add(new Employees
            {
                Id = 1,
                Name = "ApiLayerTest",
                Email = "ApiLayerTest@gmail.com",
                Age= 27
            }
            );
           
            this.employeesList.TotalResults = this.employeesList.Items.Count;
          
            this.mockEmployeeRepository.Setup(data => data.ListAsync(It.IsAny<int>())).Returns(this.EmployeeListAsync(0));
        }

        /// <summary>
        /// Create setups the manager.
        /// </summary>
        private void SetupCreateData() 
        {            
            this.mockEmployeeRepository.Setup(p => p.CreateAsync(It.IsAny<Employees>())).Returns(this.EmployeeCreateListAsync(employeeData));
        }


        /// <summary>
        /// Setups the manager.
        /// </summary>
        private void SetUpDataForDelete()
        {
            this.employeesListDelete.AssignItems(new List<Employees> { new Employees { Id = 1 }, new Employees { Id = 2 }, new Employees { Id = 6 } });
            this.employeesListDelete.TotalResults = this.employeesListDelete.Items.Count;           
            string id = Convert.ToString(this.employeesListDelete.Items.Select(employee => employee.Id), CultureInfo.CurrentCulture);
            this.mockEmployeeRepository.Setup(p => p.DeleteEmployee(2));
        }


        /// <summary>
        /// Employees the list asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The Employee List</returns>
        private Task<ICollection<Employees>> EmployeeListAsync(int id)
        {
            var retValue = new ListResult<Employees>();
            if (id >= 0)
            {
                retValue.AssignItems(this.employeesList.Items.ToList());
                retValue.TotalResults = retValue.Items.Count;
            }

            return Task.FromResult(retValue.Items);
        }


        /// <summary>
        /// Employees the list asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The Employee List</returns>
        private Task<IEnumerable<Employees>> EmployeeCreateListAsync(ICollection<Employees> employeeData)
        {
            var retValue = new ListResult<Employees>();
            if (employeeData.Count>0)
            {
                retValue.AssignItems(this.employeesList.Items.ToList());
                retValue.TotalResults = retValue.Items.Count;
            }

            return Task.FromResult(retValue.Items as IEnumerable<Employees>);
        }     

        #endregion Private Methods
    }
}
