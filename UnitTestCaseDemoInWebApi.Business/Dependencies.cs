﻿// <copyright file="Dependencies.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech </copyright>

namespace UnitTestCaseDemoInWebApi.Business
{
    #region Namespace

    using BaseService;
    using Contracts;
    using Microsoft.Practices.Unity;

    #endregion Namespace

    /// <summary>
    /// This class is a static class to register all dependencies for the project
    /// </summary>
    public static class Dependencies
    {
        #region Public Members

        /// <summary>
        /// Register dependencies of current project
        /// </summary>
        public static void Register()
        {
            DataAccess.Dependencies.Register();
            
            DIContainer.Instance.RegisterType<IEmployeeManager, EmployeeManager>();
            
        }

        #endregion Public Members
    }
}