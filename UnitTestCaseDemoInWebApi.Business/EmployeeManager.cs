﻿// <copyright file="EmployeeManager.cs" company="KelltonTech">Copyright (C) 2017 KelltonTech </copyright>

namespace UnitTestCaseDemoInWebApi.Business
{
    #region Namespace

    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Contracts;
    using DataAccess.Contracts;
    using Entity; 

    #endregion

    /// <summary>
    /// Employee Manager
    /// </summary>
    public class EmployeeManager : IEmployeeManager
    {
        #region Private Members

        /// <summary>
        /// The employee repository
        /// </summary>
        private readonly IEmployeeRepository employeeRepository;

        #endregion Private Members
        
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeManager" /> class.
        /// </summary>
        /// <param name="employeeRepository">The employee repository.</param>
        public EmployeeManager(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        #endregion Constructor

        #region Public Members

        /// <summary>
        /// Lists the asynchronous.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>
        /// Employee Details
        /// </returns>
        public async Task<ICollection<Employees>> ListAsync(int filter)
        {
              return await this.employeeRepository.ListAsync(filter);
        }

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="employee">The Employee.</param>
        /// <returns>
        /// The Employee
        /// </returns>
        public async Task<IEnumerable<Employees>> CreateAsync(Employees employee)
        {
            return await this.employeeRepository.CreateAsync(employee);
        }

        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="employee">The Employee.</param>
        /// <returns>
        /// To Update Employee
        /// </returns>
        public async Task<IEnumerable<Employees>> UpdateAsync(Employees employee)
        {
            return await this.employeeRepository.UpdateAsync(employee);
        }

        /// <summary>
        /// Deletes the employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// Nothing
        /// </returns>
        public async Task<string> DeleteEmployee(int id)
        {
            return await this.employeeRepository.DeleteEmployee(id);
        }

        #endregion
    }
}
